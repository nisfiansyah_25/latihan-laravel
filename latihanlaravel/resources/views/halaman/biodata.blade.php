<html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>media online</title>
    </head>
    <body>
        <h1>Buat account baru !</h1> 
        <h3>Sign Up Form</h3>
    <form action='/kirim' method='post'>
        @csrf
        <label>First Name</label> <br><br>
        <input type="text" name='fname'> <br><br>
        <label>Last Name</label> <br><br>
        <input type="text" name='lname'> <br><br>
        <label>Gender</label> <br>
        <input type="radio" name="gender" value="Laki-Laki"> Laki-Laki<br>
        <input type="radio" name="gender" value="Perempuan"> Perempuan<br>
        <input type="radio" name="gender" value="Others"> Others <br>
        <p>Nationality</p>
        <select name="Nationality">
            <option selected value="Inggris">Inggris</option>
            <option selected value="Amerika">Amerika</option>
            <option selected value="Indonesia">Indonesia</option>
        </select>
        <p>Language Spoken :</p>
        <input type="checkbox" name="language" value="Bahasa Indonesia"> Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="English"> English<br>
        <input type="checkbox" name="language" value="Others"> Others<br>
        <p>Biodata</p>
        <textarea cols="50" rows="7" id="bio" ></textarea> <br><br>
    
        <input type="submit" value="Sign Up">
    </form>
    </body>
</html>